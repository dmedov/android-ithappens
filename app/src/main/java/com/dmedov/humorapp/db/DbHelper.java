package com.dmedov.humorapp.db;

import android.content.Context;
import android.util.Log;

import com.dmedov.humorapp.App;
import com.dmedov.humorapp.H;
import com.dmedov.humorapp.model.Story;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by dpr on 23/09/15.
 */
public class DbHelper {
    public static void saveStories(List<Story> stories) {
        Realm realm = Realm.getInstance(App.getInstance());
        realm.beginTransaction();
        realm.clear(Story.class);
        realm.copyToRealm(stories);
        realm.commitTransaction();
        RealmResults<Story> result = realm.where(Story.class).findAll();
        H.logD("stories saved, size is: " + result.size());
    }

    public static void clearStories() {
        Realm realm = Realm.getInstance(App.getInstance());
        realm.beginTransaction();
        realm.clear(Story.class);
        realm.commitTransaction();
        H.logD("stories cleared");
    }

    public static List<Story> getStories(Context context) {
        Realm realm = Realm.getInstance(context);
        return realm.where(Story.class).findAll();
    }

    public static void updateStory(Story story, boolean isFavourite) {
        Realm realm = Realm.getInstance(App.getInstance());
        realm.beginTransaction();
        story.setIsFavourite(isFavourite);
        realm.commitTransaction();
    }
}
