package com.dmedov.humorapp.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dmedov.humorapp.H;
import com.dmedov.humorapp.R;
import com.dmedov.humorapp.adapters.StoriesAdapter;
import com.dmedov.humorapp.model.Story;
import com.dmedov.humorapp.presenters.StoriesPresenter;
import com.dmedov.humorapp.views.StoriesView;

import java.util.List;
import butterknife.ButterKnife;

/**
 * Created by dpr on 23/09/15.
 */
public class StoriesFragment extends Fragment implements StoriesView {

    StoriesPresenter storiesPresenter;
    StoriesAdapter   adapter;

    RecyclerView     recyclerView;

    @Override
    public void showStories(List<Story> stories) {
        adapter.setStories(stories);
        adapter.notifyDataSetChanged();
        H.logD("stories showed");
    }

    @Override
    public void showLoading() {
        //todo
    }

    @Override
    public void hideLoading() {
        //todo
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_stories, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(view);

        storiesPresenter = new StoriesPresenter(this);
        storiesPresenter.onCreate();

        adapter = new StoriesAdapter(getActivity());
        recyclerView = (RecyclerView)view.findViewById(R.id.list_stories);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);

        storiesPresenter.fetchStories();
    }

    @Override
    public void onResume() {
        super.onResume();
        storiesPresenter.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        storiesPresenter.onPause();
    }
}
