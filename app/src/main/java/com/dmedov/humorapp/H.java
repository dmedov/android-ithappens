package com.dmedov.humorapp;

import android.util.Log;

public class H {
    private static final String APP_TAG = "humorapp";

    public static void logD(String message) {
        Log.d(APP_TAG, message);
    }

    public static void logE(String message) {
        Log.e(APP_TAG, message);
    }
}
