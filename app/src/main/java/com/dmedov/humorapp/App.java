package com.dmedov.humorapp;

import android.app.Application;

/**
 * Created by dpr on 22/09/15.
 */
public class App extends Application {
    private static App instance;

    public static App getInstance(){
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }
}
