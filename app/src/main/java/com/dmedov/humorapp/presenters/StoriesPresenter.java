package com.dmedov.humorapp.presenters;

import com.dmedov.humorapp.App;
import com.dmedov.humorapp.H;
import com.dmedov.humorapp.api.Api;
import com.dmedov.humorapp.db.DbHelper;
import com.dmedov.humorapp.model.Story;
import com.dmedov.humorapp.views.StoriesView;

import java.util.List;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

public class StoriesPresenter extends BasePresenter {

    StoriesView storiesView;

    public StoriesPresenter(StoriesView storiesView) {
        this.storiesView = storiesView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public void fetchStories() {
        List<Story> stories = DbHelper.getStories(App.getInstance());
        if (stories.size() > 0) {
            storiesView.showStories(stories);
            H.logD("stories fetched from db");
            return;
        }

        Observable<List<Story>> listObservable = new Api().loadStories();
        listObservable.observeOn(AndroidSchedulers.mainThread())
                      .subscribe(new Action1<List<Story>>() {
            @Override
            public void call(List<Story> stories) {
                storiesView.showStories(stories);
                DbHelper.saveStories(stories);
            }
        });
    }

    public void refreshStories() {
        // todo
    }
}
