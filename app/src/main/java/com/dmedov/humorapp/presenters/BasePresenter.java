package com.dmedov.humorapp.presenters;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by dpr on 23/09/15.
 */
public abstract class BasePresenter implements  Presenter {
    CompositeSubscription compositeSubscription;

    @Override
    public void onResume() {
        compositeSubscription = new CompositeSubscription();
    }

    @Override
    public void onCreate() {
    }

    @Override
    public void onPause() {
        compositeSubscription.clear();
    }
}
