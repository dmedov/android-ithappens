package com.dmedov.humorapp.presenters;

/**
 * Created by dpr on 23/09/15.
 */
public interface Presenter {
    void onResume();
    void onCreate();
    void onPause();
}
