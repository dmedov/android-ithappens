package com.dmedov.humorapp.views;

/**
 * Created by dpr on 23/09/15.
 */
public interface LoadingView {
    void showLoading();
    void hideLoading();
}
