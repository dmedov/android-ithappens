package com.dmedov.humorapp.views;

import com.dmedov.humorapp.model.Story;

import java.util.List;

/**
 * Created by dpr on 23/09/15.
 */
public interface StoriesView extends LoadingView {
    void showStories(List<Story> stories);
}
